import numpy as np

class SmoothenClass(object):
    """
    Class for smoothening out the data 
    """
    def __init__(self, order):
        """
        Constructor. Initialize the arrays.
        
        Parameters
        ----------
        order : int
            The order to which to find the regression.
        """
        self.order = order
        self.x = np.arange(8.0)
        self.y = np.arange(8.0)


    def AddValue(self, string, index, float):
        """
        Use this function to add values to the arrays

        Parameters
        ----------
        string : str
            Specify X or Y array.
        index : int
            Where in the array to add the value.
        float : float
            The actual value that is to be added.
        """
        if string == "Y": #make y
            self.y[index] = float
        if string == "X": # make x
            self.x[index] = float

    def ReturnPrediction(self):
        """
        Return a predicted value
        
        Return
        ------
        float
            Predictor(timeLapsed)
        """
        endTime = time.time()
        timeLapsed = endTime - startTime
        #print("X Array: " + str(self.x))
        #print("Y Array: " + str(self.y))

        Regression = np.polyfit(self.x, self.y, self.order)
        Predictor = np.poly1d(Regression)
        return Predictor(timeLapsed)