.. Vision2020 documentation master file, created by
   sphinx-quickstart on Tue Apr 14 15:50:02 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Vision2020's documentation!
======================================
This web page contains the documentation for Team 1259's 2020 Vision Code
This documentation features Numpy-style docstrings

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
